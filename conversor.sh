#!/bin/bash
moeda1=$1
moeda2=$2
total=$3

cotacao=$(curl -s "https://api.exchangeratesapi.io/latest?base=$moeda1&symbols=$moeda2" | jq ".rates.$moeda2" ) 

resultado=$(python -c "print $total * $cotacao")

echo "$resultado $moeda2"